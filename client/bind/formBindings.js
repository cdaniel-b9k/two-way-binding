Template.formBindings.helpers({
	//
	// Set-up the data context wrapper for the form (#with)
	//
	'binding': function(formId, reactive) {
		reactive = (typeof reactive === 'undefined') ? true : reactive;
		var binding = (reactive) ? formBindings.findOne({
			'formId': formId
		}) : formBindings.findOne({
			'formId': formId
		}, {
			'reactive': false
		}); // ^ else
		return (binding) ? binding[formId] : binding; // ^ else
	}
});
Template.formBindings.events({
	//
	// Event triggers on the form for keyup
	// selector can change
	//
	'keyup form': function(e) {
		var formId = e.currentTarget.id; // Find the form's id=""
		var t = e.target; // Get currently active element (the input or w.e other elements accepts keyup)
		var q = {};
		q[t.id] = t.value; // Build query to find if this form exists within the bindings db
		var id = formBindings.findOne(q, {
			'_id': 1
		}) || Mongo.ObjectID(); // Get it or Make it
		Meteor.call('formBindingsUpdate',id, formId, t.id, t.value); // see server/methods.js : formBindingsUpdate()
	}
});