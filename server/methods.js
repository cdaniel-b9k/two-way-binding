Meteor.methods({
	//
	// Update data in db
	//
	'formBindingsUpdate': function(_id,formId,inputId,inputVal){
		var formBinding = formBindings.findOne({
			'formId': formId
		}) || {
			'_id': new Mongo.ObjectID(),
			'formId': formId
		}; // Get the form from the db or make it
		formBinding[formId] = formBinding[formId] || {}; // Extra prep
		var id = formBinding._id; // Doc id for reference
		var update = {}; update[formId] = {}; update[formId][inputId] = inputVal;
		_.extend(formBinding[formId],update[formId]); // merge the old/new data
		formBindings.upsert(id,formBinding); // Upsert: modify the existing or if none existant make it
	}
});