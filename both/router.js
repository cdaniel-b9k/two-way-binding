// First page
Router.route('/', {
	template: 'kandiDesigner',
	waitOn: function() {
		return Meteor.subscribe('kandiDesigner');
	},
//	data: function(){
//		return formBindings.findOne({'formId':'bind_dat_data'});
//	},
	action: function(){
		this.render();
	}
});
Router.route('/bind', {
	template: 'formBindings',
	waitOn: function() {
		return Meteor.subscribe('formBindings');
	},
//	data: function(){
//		return formBindings.findOne({'formId':'bind_dat_data'});
//	},
	action: function(){
		document.title = 'Data Binding';
		this.render();
	}
});